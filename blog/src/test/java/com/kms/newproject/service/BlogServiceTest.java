package com.kms.newproject.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.kms.newproject.main.Main;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@EnableAutoConfiguration
@IntegrationTest
@WebAppConfiguration
@EnableTransactionManagement(mode=AdviceMode.ASPECTJ, proxyTargetClass=true)
public class BlogServiceTest {


	@Autowired
	BlogService service;

	@Test
	public void testInsert() {
//		User user = new User("test@kms.com", "Tan", "Tan");
//		Blog blog = new Blog("1", user, null, "sample content" , null, new DateTime(),
//				true);
////		blog = service.saveOrUpdate(blog);
//		Blog blog2 = new Blog("2", user, null, "sample content", null , new DateTime(),
//				true);
//		blog2.setChildBlogs(new HashSet<Blog>(Arrays.asList(blog)));
//		service.saveOrUpdate(blog2);
//		Assert.assertEquals(2, service.findAll().size());
	}

}
