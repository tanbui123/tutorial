package com.kms.newproject.repo;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.kms.newproject.model.Blog;

public interface BlogRepo extends PagingAndSortingRepository<Blog, String> {
	
	public Page<Blog> findByIsPostTrue(Pageable page); 
	public Collection<Blog> findByIsPostTrue(); 

}