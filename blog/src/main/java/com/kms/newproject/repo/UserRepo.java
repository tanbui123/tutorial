package com.kms.newproject.repo;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.kms.newproject.model.User;

public interface UserRepo extends PagingAndSortingRepository<User, String> {
	
	public User findByUsernameAndPassword(String username, String password);
	
	public User findByIdAndPassword(String id, String password);
	
	public User findByUsername(String username);
	
}