package com.kms.newproject.service;


import java.util.Collection;

import com.kms.newproject.model.Blog;
import com.kms.newproject.service.CRUDService;

public interface BlogService extends CRUDService<Blog, String>{
	
	Collection<Blog> findPost(int pageIndex);
}
