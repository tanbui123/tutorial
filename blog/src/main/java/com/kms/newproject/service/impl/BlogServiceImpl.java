package com.kms.newproject.service.impl;


import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.kms.newproject.model.Blog;
import com.kms.newproject.repo.BlogRepo;
import com.kms.newproject.service.BlogService;

@Service
public class BlogServiceImpl extends CRUDServiceImpl<Blog, String> implements BlogService{
	

	@Autowired
	BlogRepo repo;
	
	@Override
	protected CrudRepository<Blog, String> getCrudRepo() {
		return repo;
	}

	@Override
	public Collection<Blog> findPost(int pageIndex) {
		if(pageIndex <0 ){
			return repo.findByIsPostTrue();
		}
		return repo.findByIsPostTrue(getPage(pageIndex, "postedDate", Sort.Direction.DESC)).getContent();
	}
}
	
