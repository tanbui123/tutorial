package com.kms.newproject.service.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

import com.kms.newproject.service.CRUDService;


public abstract class CRUDServiceImpl<T, ID extends Serializable> implements
		CRUDService<T, ID> {

	protected Log LOGGER = LogFactory.getLog(CRUDServiceImpl.class);
	
	public static final int ITEM_PER_PAGE = 5;
	protected abstract CrudRepository<T, ID> getCrudRepo();

	public T findOne(ID id) {
		return getCrudRepo().findOne(id);
	}

	public <S extends T> S saveOrUpdate(S entity) {
		return getCrudRepo().save(entity);
	}

	public void delete(ID id) {
		getCrudRepo().delete(id);
	}

	public void delete(T entity) {
		if(null != entity){
			getCrudRepo().delete(entity);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return IteratorUtils.toList(getCrudRepo().findAll().iterator());
	}
	
	protected Pageable getPage(int pageIndex, String field, Sort.Direction direction){
		Pageable page = new PageRequest(pageIndex, ITEM_PER_PAGE, getSort(direction, field));
		return page;
	}

	protected Sort getSort(Sort.Direction direction, String field){
		return new Sort(direction,field);
	}

}
