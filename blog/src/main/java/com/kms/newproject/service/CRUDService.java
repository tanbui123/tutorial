package com.kms.newproject.service;

import java.io.Serializable;
import java.util.List;

public interface CRUDService<T, ID extends Serializable> {

    T findOne(ID id);

    List<T> findAll();

    <S extends T> S saveOrUpdate(S entity);

    void delete(ID id);

    void delete(T entity);
    

}
