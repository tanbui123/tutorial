package com.kms.newproject.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.kms.newproject.model.User;
import com.kms.newproject.repo.UserRepo;
import com.kms.newproject.service.UserService;

@Service
public class UserServiceImpl extends CRUDServiceImpl<User, String> implements UserService{

	@Autowired
	UserRepo repo;
	
	@Override
	protected CrudRepository<User, String> getCrudRepo() {
		return repo;
	}

	@Override
	public boolean isExisted(String userName) {
		return repo.findByUsername(userName) != null;
	}

	@Override
	public User getUser(String username, String password) {
		return repo.findByUsernameAndPassword(username, password);
	}

	@Override
	public User getUser(String username) {
		return repo.findByUsername(username);
	}

}
