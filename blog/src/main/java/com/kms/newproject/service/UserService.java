package com.kms.newproject.service;


import com.kms.newproject.model.User;

public interface UserService extends CRUDService<User, String>{
	public boolean isExisted(String userName);
	
	public User getUser(String username, String password);
	
	public User getUser(String username);
}
