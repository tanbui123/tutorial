package com.kms.newproject.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GeneralController {
	
	protected static final Log LOGGER = LogFactory.getLog(GeneralController.class);
	
	public static final String MAIN_PAGE = "/index.html";
	
	@RequestMapping(value = "/home*", method = RequestMethod.GET)
	public ModelAndView goHome(HttpServletRequest req, HttpServletResponse res) {
		return new ModelAndView("redirect:" + MAIN_PAGE);
	}
}
