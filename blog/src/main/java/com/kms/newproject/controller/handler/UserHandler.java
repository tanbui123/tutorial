package com.kms.newproject.controller.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.kms.newproject.model.User;
import com.kms.newproject.service.UserService;
import com.kms.newproject.service.impl.UserSecurityService;
import com.kms.newproject.utils.TokenUtils;

@Component
public class UserHandler extends Handler {
	@Autowired
	private UserService userService;

	@Autowired
	private UserSecurityService userDetailsService;
	

	@Autowired
	private DaoAuthenticationProvider daoAuthenticationProvider;

	public HttpEntity<?> register(User user) {
		if (userService.isExisted(user.getUsername())) {
			return getReponse("user already existed", null,
					HttpStatus.NOT_ACCEPTABLE);
		}
		user.setId(user.getUsername());
		userService.saveOrUpdate(user);
		return getReponse(SUCCESS_MESSAGE, null, HttpStatus.OK);
	}

	public HttpEntity<?> authorize(User user) throws AuthenticationException, Exception {
		User registeredUser = userService.getUser(user.getUsername(),
				user.getPassword());
		if (null == registeredUser) {
			return getReponse("not registered user", null,
					HttpStatus.UNAUTHORIZED);
		}
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
				user.getUsername(), user.getPassword());
		daoAuthenticationProvider.setUserDetailsService(userDetailsService);
		Authentication authentication = daoAuthenticationProvider
				.authenticate(authenticationToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		return getReponse(SUCCESS_MESSAGE,
				TokenUtils.createToken(userDetailsService
						.loadUserByUsername(user.getUsername())), HttpStatus.OK);
	}


}
