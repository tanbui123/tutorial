package com.kms.newproject.controller.handler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.kms.newproject.controller.object.GeneralResponse;

@Component
public class Handler {
	
	protected static final Log LOGGER = LogFactory.getLog(Handler.class);
	
	public static final String SUCCESS_MESSAGE = "success";
	public static final String ERROR_MESSAGE = "error";
	
	@SuppressWarnings("rawtypes")
	protected final ResponseEntity<?> getReponse(String message, Object data, HttpStatus status){
		return new ResponseEntity<GeneralResponse>(
				new GeneralResponse<>(message, data), status);
	}
	
}
