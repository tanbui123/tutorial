package com.kms.newproject.controller.handler;

import java.util.Collection;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.kms.newproject.controller.object.BlogObject;
import com.kms.newproject.model.Blog;
import com.kms.newproject.model.User;
import com.kms.newproject.service.BlogService;
import com.kms.newproject.service.UserService;
import com.kms.newproject.utils.TokenUtils;

@Component
public class BlogHandler extends Handler {

	@Autowired
	UserService userService;

	@Autowired
	BlogService blogService;

	public HttpEntity<?> postOrUpdate(BlogObject blogObject, String token) {
		String id = blogObject.getId();
		String title = blogObject.getTitle();
		String content = blogObject.getContent();
		String parentId = blogObject.getParentId();
		String img = blogObject.getImg();
		Blog parentBlog = null;
		Blog blog = null;
		String userName = TokenUtils.getUserNameFromToken(token);
		User user = userService.getUser(userName);
		LOGGER.info(user);
		// post a new blog
		if (StringUtils.isEmpty(id) && StringUtils.isEmpty(parentId) && user.isAdmin()) {
			blog = blogService.saveOrUpdate(new Blog(new ObjectId().toString(),
					user, title, content, img,new DateTime(), true));
			return getReponse(SUCCESS_MESSAGE, blog, HttpStatus.ACCEPTED);
		}
		// post a reply
		if (StringUtils.isEmpty(id) && !StringUtils.isEmpty(parentId)) {
			blog = blogService.saveOrUpdate(new Blog(new ObjectId().toString(),
					user, title, content, null,new DateTime(), false));
			parentBlog = blogService.findOne(parentId);
			parentBlog.getChildBlogs().add(blog);
			blogService.saveOrUpdate(parentBlog);
			return getReponse(SUCCESS_MESSAGE, blog, HttpStatus.ACCEPTED);
		}
		// edit a blog
		if (!StringUtils.isEmpty(id) && StringUtils.isEmpty(parentId) && user.isAdmin() ) {
			blog = blogService.findOne(id);
			blog.setContent(content);
			blog.setTitle(title);
			blog.setPostedDate(new DateTime());
			return getReponse(SUCCESS_MESSAGE, blogService.saveOrUpdate(blog), HttpStatus.OK);
		}
		// edit a reply
		if (!StringUtils.isEmpty(id) && !StringUtils.isEmpty(parentId)) {

		}
		return getReponse(ERROR_MESSAGE, blog, HttpStatus.FORBIDDEN);
	}

	public HttpEntity<?> get(int pageIndex) {
		Collection<?> blogs = blogService.findPost(pageIndex);
		return getReponse(SUCCESS_MESSAGE, blogs, HttpStatus.OK);
	}

	public HttpEntity<?> getNumberBlog() {
		Collection<?> blogs = blogService.findPost(-1);
		return getReponse(SUCCESS_MESSAGE, Integer.valueOf(blogs.size()),
				HttpStatus.OK);
	}

	public HttpEntity<?> get(String id) {
		Blog blog = blogService.findOne(id);
		return new ResponseEntity<Blog>(blog, HttpStatus.OK);
	}

	public HttpEntity<?> delete(String token, String id) {
		blogService.delete(id);
		return getReponse(SUCCESS_MESSAGE, null, HttpStatus.OK);
	}

	public HttpEntity<?> like(String blogId, String token) {
		String username = TokenUtils.getUserNameFromToken(token);
		Blog blog = blogService.findOne(blogId);
		if(blog == null){
			return getReponse(ERROR_MESSAGE, null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		Set<String> likes = blog.getLikes();
		if(likes.contains(username)){
			return getReponse("already liked", null, HttpStatus.NOT_ACCEPTABLE);
		}
		blog.getLikes().add(username);
		return getReponse(SUCCESS_MESSAGE, blogService.saveOrUpdate(blog), HttpStatus.OK);
	}

}
