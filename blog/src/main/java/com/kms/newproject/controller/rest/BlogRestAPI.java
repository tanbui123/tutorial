package com.kms.newproject.controller.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.access.annotation.Secured;
//import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kms.newproject.controller.handler.BlogHandler;
import com.kms.newproject.controller.object.BlogObject;

@Controller
@RequestMapping("/api/")
public class BlogRestAPI {

	@Autowired
	private BlogHandler blogHandler;

	protected static final Log LOGGER = LogFactory.getLog(BlogRestAPI.class);

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = { "blog" }, method = RequestMethod.POST)
	@ResponseBody
	public HttpEntity<?> post(@RequestBody BlogObject blogObject,
			@RequestHeader("Authorization") String token) {
		return blogHandler.postOrUpdate(blogObject, token);
	}
	
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	@RequestMapping(value = { "blog/{id}" }, method = RequestMethod.POST)
	@ResponseBody
	public HttpEntity<?> updateBlog(@RequestBody BlogObject blogObject,
			@RequestHeader("Authorization") String token) {
		return blogHandler.postOrUpdate(blogObject, token);
	}

	@RequestMapping(value = { "blog" }, method = RequestMethod.GET)
	@ResponseBody
	public HttpEntity<?> get(@RequestParam int pageIndex) {
		return blogHandler.get(pageIndex);
	}

	@RequestMapping(value = { "total" }, method = RequestMethod.GET)
	@ResponseBody
	public HttpEntity<?> getNumber() {
		return blogHandler.getNumberBlog();
	}

	@RequestMapping(value = { "blog/{id}" }, method = RequestMethod.DELETE)
	@ResponseBody
	public HttpEntity<?> delete(@RequestHeader("Authorization") String token,
			@PathVariable("id") String id) {
		return blogHandler.delete(token, id);
	}

	@RequestMapping(value = { "blog/{id}" }, method = RequestMethod.GET)
	@ResponseBody
	public HttpEntity<?> getBlog(
			@PathVariable("id") String id) {
		return blogHandler.get(id);
	}
	
	
	@RequestMapping(value = {"like/{blogId}"}, method = RequestMethod.GET)
	@ResponseBody
	public HttpEntity<?> like(@PathVariable String blogId,@RequestHeader("Authorization")String token){
		return blogHandler.like(blogId, token);
	}
}
