package com.kms.newproject.controller.object;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BlogObject {
	
	public BlogObject(){
		
	}
	public BlogObject(String id, String title, String content, String parentId, String img){
		this.id = id;
		this.title = title;
		this.content = content;
		this.parentId = parentId;
		this.img = img;
	}
	
	@JsonProperty
	String id;
	@JsonProperty
	private String content;
	@JsonProperty
	private String title;
	@JsonProperty
	private String parentId;
	@JsonProperty
	private String img;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	
	
}
