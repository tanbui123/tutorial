package com.kms.newproject.controller.object;

public class GeneralResponse<T> {
	
	public T data;
	public GeneralResponse(String message, T data){
		this.message = message;
		this.data = data;
	}
	
	public GeneralResponse(){
	}
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
}
