package com.kms.newproject.controller.rest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kms.newproject.controller.handler.UserHandler;
import com.kms.newproject.model.User;

@Controller
@RequestMapping("/api/user")
public class UserRestAPI {

	@Autowired
	private UserHandler userHandler;

	protected static final Log LOGGER = LogFactory.getLog(UserRestAPI.class) ;

	@RequestMapping(value = { "register" }, method = RequestMethod.POST)
	@ResponseBody
	public HttpEntity<?> register(@RequestBody User user) {
		return userHandler.register(user);
	}
	
	@RequestMapping(value = { "authenticate" }, method = RequestMethod.POST)
	@ResponseBody
	public HttpEntity<?> authenticate(@RequestBody User user) throws AuthenticationException, Exception {
		return userHandler.authorize(user);
	}
	
	public HttpEntity<?> logOut(@RequestHeader String token) {
//		return userHandler.logOut(token);
		return null;
	}
	

}
