package com.kms.newproject.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.kms.newproject.security.AuthenticationTokenProcessingFilter;
import com.kms.newproject.security.RestAuthenticationEntryPoint;
import com.kms.newproject.security.RestAuthenticationSuccessHandler;
import com.kms.newproject.service.impl.UserSecurityService;



@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter  {
	@Autowired
	@Qualifier("userDetailsService")
	UserSecurityService userSecurityService;
	@Autowired
	RestAuthenticationEntryPoint restAuthenticationEntryPoint;
	@Autowired
	RestAuthenticationSuccessHandler restAuthenticationSuccessHandler;
	@Autowired
	AuthenticationTokenProcessingFilter filter;
	
	@Bean
	 public AuthenticationProvider authenticationProvider(){
	  DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
	  daoAuthenticationProvider.setUserDetailsService(userSecurityService);
	  daoAuthenticationProvider.setPasswordEncoder(new PlaintextPasswordEncoder());
	  return daoAuthenticationProvider;
	 }
	
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth)
			throws Exception {
		auth.authenticationProvider(authenticationProvider());
		auth.userDetailsService(userSecurityService);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and().exceptionHandling()
					.authenticationEntryPoint(restAuthenticationEntryPoint);
		http.csrf().disable();
		http.authorizeRequests()
		.antMatchers("/","/index.html", "/api/total**" ,"/api/user/authenticate").permitAll()
		.antMatchers(HttpMethod.GET, "/api/blog**").permitAll()
		.antMatchers(HttpMethod.GET, "/api/blog/*").permitAll()
		.antMatchers(HttpMethod.POST,"/api/blog").hasAnyRole("ADMIN", "USER")
		.antMatchers(HttpMethod.POST,"/api/blog").hasAnyRole("ADMIN", "USER")
		.anyRequest().authenticated();
//							.antMatchers("*").hasRole("ADMIN")
		
//		
//		.and().formLogin()
//		.successHandler(restAuthenticationSuccessHandler)
//		  .loginPage("/login")
//		  .loginProcessingUrl("api/user/authenticate")
//		  .defaultSuccessUrl("/")
//		  .failureUrl("/login")
//		  .usernameParameter("email")
//		  .passwordParameter("password")
//		  .permitAll(true);
//		http.authorizeRequests().anyRequest().fullyAuthenticated();
//		http.httpBasic();
//		http.csrf().disable();
		http.addFilterBefore(filter,UsernamePasswordAuthenticationFilter.class);
//		http.logout().logoutUrl("/api/user/logout");
	}
	@Override
	public void configure(WebSecurity web) throws Exception {
		web
        .ignoring().antMatchers("/app/**", "images/**")
           .antMatchers("/app/**")
           .antMatchers(HttpMethod.GET, "/api/blog*")
		   .antMatchers("**/total**")
		   .antMatchers("/images/**");
			
	}
	

}