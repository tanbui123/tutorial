package com.kms.newproject.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kms.newproject.utils.SimpleDatetimeSerializer;
 
@Document(collection = "blog")
public class Blog implements Comparable<Blog> {
	
	public Blog(){
		this.childBlogs = new HashSet<>();
		this.likes = new HashSet<>();
	}
	
	public Blog(String id, User user, String title, String content, String img, DateTime postedDate, boolean isPost){
		this.id = id;
		this.user = user;
		this.title = title;
		this.content = content;
		this.img = img;
		this.postedDate = postedDate;
		this.isPost = isPost;
		this.childBlogs = new HashSet<>();
	}
	
	private String img;
	
	@Id
	private String id;
	
	private boolean isPost;
	
	@DBRef
	private User user;
	
	private Set<String> likes;
	
	@DBRef
	private Set<Blog> childBlogs;
	
	private DateTime postedDate;
 
	private String content;
	
	private String title;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Collection<Blog> getChildBlogs() {
		return childBlogs;
	}

	public void setChildBlogs(Set<Blog> childBlogs) {
		this.childBlogs = childBlogs;
	}

	@JsonSerialize(using=SimpleDatetimeSerializer.class)
	public DateTime getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(DateTime postedDate) {
		this.postedDate = postedDate;
	}
	

	public boolean isPost() {
		return isPost;
	}

	public void setPost(boolean isPost) {
		this.isPost = isPost;
	}

	@Override
	public int compareTo(Blog o) {
		return o.postedDate.compareTo(this.postedDate);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Set<String> getLikes() {
		return likes;
	}

	public void setLikes(Set<String> likes) {
		this.likes = likes;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public int hashCode() {
		int prime = 37;
		int result = 3;
		return prime* result + id.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		if(!(obj instanceof Blog)){
			return false;
		}
		Blog object = (Blog) obj;
		return this.hashCode() == object.hashCode();
	}
}