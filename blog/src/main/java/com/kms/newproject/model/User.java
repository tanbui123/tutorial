package com.kms.newproject.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "user")
public class User {
	
	public User(){}
	
	public User(String username, String password, String name){
		this.id = username;
		this.username = username;
		this.password = password;
		this.name = name;
	}
	
	@JsonProperty()
	@Id
	private String id;
	@JsonProperty("email")
	@Indexed
	private String username;
	@JsonProperty("name")
	private String name;
	@JsonProperty("password")
	private String password;
	@JsonProperty("role")
	private int role;
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setId(String id){
		this.id = id;
	}
	
	public String getId(){
		return this.username;
	}
	public int getRole() {
		return role;
	}
	
	public void setRole(int role) {
		this.role = role;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public boolean isAdmin(){
		return this.role == 0;
	}
	
	
	
	
	
}
