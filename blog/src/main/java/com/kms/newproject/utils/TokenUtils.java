package com.kms.newproject.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;


public class TokenUtils
{

	public static final String MAGIC_KEY = "something";
	public static final String TOKEN_SEPARATOR = ":";

	public static String createToken(UserDetails userDetails)
	{
		/* Expires in 10 minutes */
		long expires = System.currentTimeMillis() + 1000L * 60 * 20;

		StringBuilder tokenBuilder = new StringBuilder();
		tokenBuilder.append(userDetails.getUsername());
		tokenBuilder.append(TOKEN_SEPARATOR);
		tokenBuilder.append(expires);
		tokenBuilder.append(TOKEN_SEPARATOR);
		tokenBuilder.append(TokenUtils.computeSignature(userDetails, expires));
		return tokenBuilder.toString();
	}


	public static String computeSignature(UserDetails userDetails, long expires)
	{
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(userDetails.getUsername());
		signatureBuilder.append(TOKEN_SEPARATOR);
		signatureBuilder.append(expires);
		signatureBuilder.append(TOKEN_SEPARATOR);
		signatureBuilder.append(userDetails.getPassword());
		signatureBuilder.append(TOKEN_SEPARATOR);
		signatureBuilder.append(TokenUtils.MAGIC_KEY);

		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}

		return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
	}


	public static String getUserNameFromToken(String authToken)
	{
		if (null == authToken) {
			return null;
		}

		String[] parts = authToken.split(":");
		return parts[0];
	}


	public static boolean validateToken(String authToken, UserDetails userDetails)
	{
		if(!StringUtils.contains(authToken, TOKEN_SEPARATOR)){
			return false;
		};
		String[] parts = authToken.split(TOKEN_SEPARATOR);
		long expires = Long.parseLong(parts[1]);
		String signature = parts[2];

		if (expires < System.currentTimeMillis()) {
			return false;
		}

		return signature.equals(TokenUtils.computeSignature(userDetails, expires));
	}
}