var app = angular.module("myBlogApp", [ 'ngRoute', 'ngResource',
		'ui.bootstrap', 'ngCookies' ]);

app.constant('CONSTANTS', {
	TOKEN : "a"
});

app.run([ '$http', 'CONSTANTS', function($http, CONSTANT) {
	// $http.defaults.headers.common.authorization = CONSTANT.TOKEN;
} ]);

app.constant('USER', {
	user : 'user'
});

app.config([ '$routeProvider', '$httpProvider',
		function($routeProvider, $httpProvider, $cookieStore) {
			$routeProvider.otherwise({
				redirectTo : '/home'
			}).when('/home', {
				templateUrl : 'app/template/home.html',
				controller : 'blogController'
			}).when('/post', {
				templateUrl : 'app/template/post.html',
				controller : 'postController'
			}).when('/view/:id', {
				templateUrl : 'app/template/view.html',
				controller : 'viewController'
			}).when('/login', {
				templateUrl : 'app/template/login.html',
				controller : 'userController'
			}).when('/signup', {
				templateUrl : 'app/template/signup.html',
				controller : 'userController'
			}).when('/edit/:id', {
				templateUrl : 'app/template/edit.html',
				controller : 'editController'
			}).when('/403', {
				templateUrl : 'app/template/403.html',
				controller : 'userController'
			});


			$httpProvider.interceptors.push('httpInterceptor');
		} ]);

app.constant('APIs', {
	LOCAL_DEV_URL : "http://localhost:8089/blog/",
	// USER
	USER_REGISTER : "api/user/register",
	USER_AUTHENTICATE : "api/user/authenticate",

	// BLOG
	BLOB_API : "api/blog/:id",
	BLOG_TOTAL : "api/total",
	BLOG_LIKE : "api/like/:id",
		
	UPLOAD_FILE : "uploadFile"

});

app.factory('httpInterceptor', function httpInterceptor($q, $location) {

	return {
		response : function(response) {
			return response;
		},
		responseError : function(response) {
			console.log('error :' + response);
			if (response.status === 400 || response.status === 401) {
				$location.path('/login');
			}
			if (response.status === 403) {
				$location.path('/403');
			}
			// do something on error
			return $q.reject(response);
		}
	};
});
