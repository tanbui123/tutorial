'use strict';

app.factory('blogService', [ 'APIs', '$resource', function(APIs, $resource) {
	return $resource(APIs.LOCAL_DEV_URL + APIs.BLOB_API, { id: "@id"}, {
	});
}]);