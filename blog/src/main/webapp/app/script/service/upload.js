'use strict';
app.service('fileUpload', ['$q', '$http', 'APIs', function ($q, $http, APIs) {
    this.uploadFileToUrl = function(file){
        var fd = new FormData();
        fd.append('file', file);
        var deferred =  $q.defer();
        $http.post(APIs.LOCAL_DEV_URL + APIs.UPLOAD_FILE, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function(data){
        	deferred.resolve(data);
        })
        .error(function(){
        	deferred.reject();
        });
        return deferred.promise;
    }
}]);