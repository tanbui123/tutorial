'use strict';

app
		.controller(
				'mainController',['$http','$scope', function($http, $scope){
					$scope.logOut = function(){
						$http.defaults.headers.common.authorization = null;
					};
					$scope.isLoggedIn = function(){
						if($http.defaults.headers.common.authorization){
							return true;
						}
						return false;
					};
				}]);