'use strict';

app.controller('postController', [
		'$http',
		'$scope',
		'blogService',
		'fileUpload',
		'$location',
		'$modal',
		function($http, $scope, blogService, fileUpload, $location, $modal, $log) {
			$scope.post = function() {
				var authToken = $http.defaults.headers.common.authorization;
				doPost();
			};

			function openDialog() {
				return $modal.open({
					templateUrl : 'app/modal/login.html',
					controller : 'userController',
				});
			}

			function doPost() {
				console.log('token :'
						+ $http.defaults.headers.common.authorization);
				var file = $scope.myFile;
				fileUpload.uploadFileToUrl(file).then(function(data){
					console.log('data : ' + data);	
					$scope.blog.img = data;
					blogService.save($scope.blog).$promise.then(function(data) {
						console.log('sucesss');
						$scope.myClass = 'show';
						setTimeout(function() {
							console.log(data);
							$location.path('/home');
							$scope.$apply();
						}, 10);
					}, function(error) {
						console.log('error');
						console.log(error.status);
					});
				});
				
			}
			;
		}

]);
