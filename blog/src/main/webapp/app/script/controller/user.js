'use strict';

app.controller('userController', ['$http', '$scope', 'userService' ,'authenService', '$location','$modal','$timeout','$window',
                                  function($http, $scope, userService, authenService, $location, $modal, $timeout, $window) {
		$scope.createUser = function(){
			userService.save($scope.user)
			// success handler
			.$promise.then(function(data) {
				console.log('sucesss');
				$scope.myClass = 'show';
				setTimeout(function(){
					console.log(data);
					$location.path('/login');
					$scope.$apply();
				}, 2000);
				
			 // error handler  
			}, function(error) {
			    console.log('error');
			    console.log(error.status);
			});;
			
		};
		
		$scope.authen = function(){
			authenService.save($scope.user)
				.$promise.then(function(data){
					var token = data.data;
					console.log('data :' + token);
					$http.defaults.headers.common.authorization = token;
					$timeout(function(){
						$window.history.back();
					},1000,true);
				}, function(error){
					$scope.myClass = "show";
					$timeout(function(){
						$scope.myClass = "hide";
					},1500,true);
				});
		};
	}]);


