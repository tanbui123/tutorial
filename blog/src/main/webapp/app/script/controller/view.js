'use strict';


app.controller('viewController', ['$http', '$routeParams', '$scope', 'blogService', '$location', 'likeService', '$timeout',
                                  function($http, $routeParams, $scope, blogService, $location, likeService, $timeout) {
	$scope.commentId = null;
	var refresh = function(){
		$scope.blog = blogService.get({id: $routeParams.id});
		console.log($scope.blog);
		$scope.commentId = null;
		$scope.reply = null;
		$scope.comment = null;
	};
	refresh();
	
	$scope.like = function(blogId){
		likeService.get({
			id : $routeParams.id
		}).$promise.then(function(data){
			refresh();
		})
	};
	
	
	$scope.postComment = function(){
		var comment = {};
		comment.parentId = $scope.blog.id;
		comment.content = $scope.comment;
		if($scope.comment){
			blogService.save(comment).$promise.then(function(data){
				console.log('sucesss');
				refresh();
			});
		};
	};
	
	
	
	$scope.showReplyBox = function(commentId){
		$scope.commentId = commentId;
		$scope.showForm = true;
		$scope.focusInput = true;
	};
	
	$scope.hideReplyBox = function(){
		$scope.showForm = false;
		var comment = {};
		comment.parentId = $scope.commentId;
		comment.content = $scope.reply;
		if($scope.reply){
			blogService.save(comment).$promise.then(function(data){
				console.log('sucesss');
				refresh();
			});
		}
	};
	
	
	$scope.$watch('comment',function(newVal, oldVal){
		//console.log(newVal);
		if(newVal === undefined || newVal ==''){
			$scope.myClass = 'show';
		}
		else{
			$scope.myClass = 'hide';
		}
	});
		
	}]);

