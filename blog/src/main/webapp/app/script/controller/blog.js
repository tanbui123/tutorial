'use strict';

app
		.controller(
				'blogController',
				[
						'$http',
						'$scope',
						'dataService',
						'blogService',
						'utilService',
						'$location',
						'likeService',
						function($http, $scope, dataService, blogService,
								utilService, $location, likeService) {
							// redirect to compose page
							$scope.compose = function() {
								$location.path('/post');
							};
							$scope.edit = function(blogId){
								$location.path('/edit/' + blogId);
							}
							
							$scope.numPerPage = 5, $scope.currentPage = 0;
							$scope.maxSize = 5;

							$scope.showReplyBox = function(commentId) {
								$scope.showForm = true;
								$scope.focusInput = true;
							};

							$scope.hideReplyBox = function() {
								$scope.showForm = false;
							};

							$scope.numPages = function() {
								return Math.ceil($scope.blogs.length
										/ $scope.numPerPage);
							};

							$scope.setPage = function(pageNo) {
								$scope.currentPage = pageNo;
							};

							init();

							$scope.numPerPage = 5;
							$scope.currentPage = 0;

							$scope.prevPage = function() {
								if ($scope.currentPage > 0) {
									$scope.currentPage--;
								}
							};

							$scope.prevPageDisabled = function() {
								return $scope.currentPage === 0 ? "disabled"
										: "";
							};

							$scope.nextPage = function() {
								if ($scope.currentPage < $scope.pageCount() - 1) {
									$scope.currentPage++;
								}
							};

							$scope.nextPageDisabled = function() {
								return $scope.currentPage === $scope
										.pageCount() - 1 ? "disabled" : "";
							};

							$scope.pageCount = function() {
								var x = Math.ceil($scope.total
										/ $scope.numPerPage);
								return x;
							};

							$scope.setPage = function(n) {
								if (n > 0 && n < $scope.pageCount()) {
									$scope.currentPage = n;
								}
							};

							function init() {
								console.log('initilizing....');
								utilService.get().$promise.then(function(data) {
									console.log(data);
									$scope.total = data.data;
								});
								console.log('token : ' + $http.defaults.headers.common.authorization);
								getBlogs(0);

							}
							;
							$scope.$watch('currentPage', function(newVal,
									oldVal) {
								console.log(newVal, oldVal);
								if (oldVal === newVal) {
									return;
								}
								getBlogs(newVal);
							});

							function getBlogs(index) {
								blogService.get({
									'pageIndex' : index
								}).$promise.then(function(data) {
									$scope.filteredBlogs = data.data;
								});
							}
							//
						} ]);
