'use strict';

app.controller('editController',
		[
				'$http',
				'$routeParams',
				'$scope',
				'blogService',
				'$location',
				'$timeout',
				function($http, $routeParams, $scope, blogService, $location,
						$timeout) {
					$scope.commentId = null;
					var refresh = function() {
						$scope.blog = blogService.get({
							id : $routeParams.id
						});
						console.log($scope.blog);
						$scope.commentId = null;
						$scope.reply = null;
						$scope.comment = null;
					};
					refresh();

					$scope.edit = function() {
						blogService.save($scope.blog).$promise.then(function(data) {
							console.log('sucesss');
							refresh();
						});
					};

					$scope.remove = function() {
						if ($scope.comment) {
							blogService.remove(blog).$promise.then(function(
									data) {
								console.log('sucesss');
								refresh();
							});
						}
						;
					};

					$scope.isPost = function() {
						if ($scope.blog.title) {
							return true;
						}
						return false;
						;
					};

				} ]);
